/*
    This is the main entry into application.
    We create ankyb modules and added its dependencies.

*/
ankyb = angular.module('Ankyb', ['ngAnimate', 'ui.bootstrap', 'ankyb.controllers', 'ankyb.services',
                                 'ngMessages', 'ankyb.directives']);