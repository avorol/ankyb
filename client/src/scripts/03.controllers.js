ankybControllers = angular.module('ankyb.controllers', []);


ankybControllers
.controller('MainCtrl', function () {
})
.controller('WidgetCtrl', function ($scope) {
    $scope.addPicture = function () {
        return {'type': 'image', 'pictureUrl': $scope.pictureUrl};
    }
})