ankybDirectives = angular.module('ankyb.directives', []);

ankybDirectives
.directive('objDraggable', [function () {
    return {
        restrict: 'A',
        link: function (scope, ele, attrs, ctrl) {
            var draggableEle = angular.element(ele);
            draggableEle.attr('draggable', 'true')

            ele.bind('drag', function (e) {
                e.preventDefault();
                draggableEle.css('left', e.clientX);
                draggableEle.css('top', e.clientY);
            });

            ele.bind('dragstart', function (e) {
//                e.dataTransfer.setData('text', eleId);
            });

            ele.bind('dragend', function (e) {
            })
        }
    }
}])
.directive('contentArea', function () {
    return {
        restrict: 'A',
        scope: {
            onDrop: '&'
        },
        link: function (scope, ele, attrs, ctrl) {
            ele.bind('dragover', function (e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }

                if (e.stopPropagation) {
                    e.stopPropagation();
                }

                e.dataTransfer.dropEffect = 'move';
                return false;
            });

            ele.bind('dragenter', function (e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
            });

            ele.bind('dragleave', function (e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
            });

            ele.bind('drop', function (e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }

                if (e.stopPropagation) {
                    e.stopPropagation();
                }

                var data = e.dataTransfer.getData('text');
                var draggableEle = document.getElementById(data);
            });
        }
    }
})
.directive('createNewPage', ['UUID', function (uuid) {
    return {
        restrict: 'A',
        link: function (scope, ele, attrs, ctrl) {
            var contentArea = document.getElementById('content-area');
            ele.bind('click', function (e) {
                e.preventDefault();

                var page = document.createElement('div');
                // TODO: css animation for page.
                page.setAttribute('class', 'page');
                contentArea.appendChild(page);
            });
        }
    }
}])
.directive('moreWidgets', function () {
    return {
        restrict: 'E',
        link: function (scope, ele, atttrs, ctrl) {
            scope.page = 'objects';
            var pageNavs = angular.element('.page-nav');
            pageNavs.bind('click', function (e) {
                e.preventDefault();

                scope.$apply(function () {
                    scope.page = angular.element(e.target).attr('data-page');
                });
            });
        }
    }
})
.directive('navPage', function () {
    /*
    *   help navigate through pages.
    *
    */
    return {
        restrict: 'A',
        link: function (scope, ele, attrs, ctrl) {

        }
    }
})
.directive('selectElement', function () {
    /*
    *   We would handle changing of element properties here when ever an element is selected.
    *
    */
    return {
        restrict: 'A',
        link: function (scope, ele, attrs, ctrl) {

        }
    }
})
.directive('objectTest', function () {
    return {
        restrict: 'E',
        template: '<div obj-draggable style="background: red; color: white; padding: 200px; z-index: 3000;" class="text-center">' +
                        'Drag me.' +
                  '</div>',
        link: function(scope, ele, attrs, ctrl) {
        }
    }
})
.directive('addContent', ['UUID', '$compile', '$rootScope', function (uuid, $compile, $rootScope) {
    return {
        restrict: 'A',
        scope: {
            fetchContent: '&addContent'
        },
        link: function (scope, ele, attrs, ctrl) {
            ele.find('button[type="submit"]').bind('click', function () {

                var contentArea = angular.element('#content-area');
                var content = scope.fetchContent();
                var contentId = uuid.new();

                if (content.type === 'image') {
                    var img = new Image();
                    var imgWidth, imgHeight;
                    img.onload = function () {
                        imgWidth = this.width;
                        imgHeight = this.height;
                        var newEle = angular.element('<div obj-draggable data-id=\''+ contentId +'\'></div>');
                        newEle.css({
                            'background-image': 'url(' + content.pictureUrl + ')',
                            'height': imgHeight,
                            'width': imgWidth
                        });
                        contentArea.append(newEle);
                        $scope = $rootScope.$new();
                        $compile(newEle)($scope);
                    }
                    img.src = content.pictureUrl;
                } else {
                }
            });
        }
    }
}])