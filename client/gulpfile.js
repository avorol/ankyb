/*
    We have defined task which processes most of our client files for better performance on the browser client.

*/


var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify')
    livereload = require('gulp-livereload'),
    jshint = require('gulp-jshint'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    gulpServe = require('gulp-serve');

gulp.task('styles', function () {
    /**
    *   We process our all sass file and generate two css file a normal files, normal css for local development,
    *   and minified version for production.
    **/
    return sass('src/styles/*.scss', { style: 'expanded' })
        .pipe(autoprefixer('last 2 version'))
        .pipe(gulp.dest('css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('css'))
});

gulp.task('scripts', function () {
    /**
    *   we compile all our js files in src/scripts to create 2 files main.js for development and a
    *   minified version main.min.js for production.
    **/
    return gulp.src('src/scripts/**/*.js')
        .pipe(jshint.reporter('default'))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('js'))
});

gulp.task('process', function () {
    gulp.watch('src/styles/**/*.scss', ['styles']);
    gulp.watch('src/scripts/**/*.js', ['scripts']);
});

gulp.task('default', ['process']);
