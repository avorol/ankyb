from flask_restful import Resource, abort
from server.models.year_book import YearBookInfo, YearBookContent
from server.commons.utils import authorize


class YearBookResource(Resource):
    def get(self):
        pass

    @authorize
    @YearBookInfo.method()
    def post(self, year_book):
        pass

    def put(self):
        pass

    def delete(self):
        pass


class YearBookContentResources(Resource):
    def get(self):
        pass

    def post(self):
        pass

    def put(self):
        pass

    def delete(self):
        pass
