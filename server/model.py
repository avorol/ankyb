from google.appengine.ext import ndb


class GraduatingClasses(ndb.Model):
    graduating_class_name = ndb.StringProperty(required=True)
    school = ndb.StringProperty(required=True)
    username = ndb.StringProperty(required=True)
    password = ndb.StringProperty(required=True)
    date_registered = ndb.DateTimeProperty(auto_now_add=True)
    date_last_updated = ndb.DateTimeProperty(auto_now=True)


class StudentProfiles(ndb.Model):
    graduating_class = ndb.KeyProperty(required=True)
    name = ndb.StringProperty(required=True)
    date_registered = ndb.DateTimeProperty(auto_now_add=True)
    date_last_updated = ndb.DateTimeProperty(auto_now=True)


class YearBooks(ndb.Model):
    pass
