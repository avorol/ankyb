from flask import request, abort
from functools import wraps


def authorize(func):
    @wraps(func)
    def decorator_wrapper():
        auth_token = request
        if auth_token:
            pass
        abort(401, 'Unauthorized request')

    return decorator_wrapper
