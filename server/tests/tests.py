import unittest
import webtest
from google.appengine.ext import testbed
from webapp2_extras import json
from server.main import app

USER_PATH = '/users'
USER = {'full_name': 'Olajide Obasan', 'email': 'jideobs@gmail.com', 'password': 'mychora',
        'confirm_password': 'mychora'}
CLINIC = {'name': 'Dental clinic', 'email': 'dentalclinic@gmail.com', 'password': 'mychora',
          'confirm_password': 'mychora'}

USER_PROFILE_DATA = {'phone_number': '08091607291', 'sex': 'Male',
                     'address': '1, Badmus Close Owutu, Ikorodu, Lagos, Nigeria', 'state': 'Lagos', 'city': 'Mushin',
                     'religion': 'Others', 'occupation': 'Software Engineer', 'marital_status': 'Single'}


class TestCasesBase(unittest.TestCase):
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_memcache_stub()
        self.testbed.init_datastore_v3_stub()
        self.testapp = webtest.TestApp(app)

    def tearDown(self):
        self.testbed.deactivate()

    def executeReq(self, path, method='post', data=None, cont_type='json', expected_status=200):
        if cont_type == 'form':
            content_type = 'application/x-www-form-urlencoded'
        else:
            content_type = 'application/json;charset=utf-8'

        json_data = json.encode(data) if 'json' in content_type else data
        if method == 'post':
            return self.testapp.post(path, params=json_data, content_type=content_type, status=expected_status)
        elif method == 'put':
            return self.testapp.put(path, params=json_data, content_type=content_type, status=expected_status)
        elif method == 'delete':
            return self.testapp.delete(path, status=expected_status)
        elif method == 'head':
            return self.testapp.head(path, status=expected_status)
        elif method == 'get':
            return self.testapp.get(path, status=expected_status)


class RegisterLoginTestCases(TestCasesBase):
    def testUserRegister(self):
        res = self.executeReq('/register-user', data=USER, cont_type='form', expected_status=302)
        self.assertEqual(res.status_int, 302)

    def testAlreadyRegisteredUser(self):
        self.executeReq('/register-user', data=USER, cont_type='form', expected_status=302)
        res = self.executeReq('/register-user', data=USER, cont_type='form')
        self.assertEqual(res.status_int, 200)

    def testClinicRegister(self):
        res = self.executeReq('/register-clinic', data=CLINIC, cont_type='form', expected_status=302)
        self.assertEqual(res.status_int, 302)

    def testAlreadyRegisteredClinic(self):
        self.executeReq('/register-clinic', data=CLINIC, cont_type='form', expected_status=302)
        res = self.executeReq('/register-clinic', data=CLINIC, cont_type='form')
        self.assertEqual(res.status_int, 200)

    def testUserLogin(self):
        self.executeReq('/register-user', data=USER, cont_type='form', expected_status=302)
        user_data = {'email': USER['email'], 'password': USER['password']}
        res = self.executeReq('/login', data=user_data, cont_type='form', expected_status=302)
        self.assertEqual(res.status_int, 302)

    def testInvalidUserLogin(self):
        user_data = {'email': 'invalidemail@gmail.com', 'password': 'newlife'}
        res = self.executeReq('/login', data=user_data, cont_type='form')
        self.assertEqual(res.status_int, 200)

    def testInvalidPasswordLogin(self):
        self.executeReq('/register-user', data=USER, cont_type='form', expected_status=302)
        user_data = {'email': USER['email'], 'password': 'invalid_pass'}
        res = self.executeReq('/login', data=user_data, cont_type='form')
        self.assertEqual(res.status_int, 200)

    def testClinicLogin(self):
        self.executeReq('/register-clinic', data=CLINIC, cont_type='form', expected_status=302)
        login_data = {'email': CLINIC['email'], 'password': CLINIC['password']}
        res = self.executeReq('/login', data=login_data, cont_type='form', expected_status=302)
        self.assertEqual(res.status_int, 302)

    def testInvalidClinicLogin(self):
        login_data = {'email': 'invalid_user', 'password': 'invalid_password'}
        self.executeReq('/login', data=login_data, cont_type='form')

    def testInvalidClinicPassword(self):
        self.executeReq('/register-clinic', data=CLINIC, cont_type='form', expected_status=302)
        login_data = {'email': CLINIC['email'], 'password': 'invalid_password'}
        self.executeReq('/login', data=login_data, cont_type='form')

    def testLogout(self):
        self.executeReq('/register-user', data=USER, cont_type='form', expected_status=302)
        login_data = {'email': USER['email'], 'password': USER['password']}
        self.executeReq('/login', data=login_data, cont_type='form', expected_status=302)
        self.executeReq('/logout', method='get', expected_status=302)


class UserResourcesTestCases(TestCasesBase):
    def setUp(self):
        super(UserResourcesTestCases, self).setUp()
        self.executeReq('/register-user', data=USER, cont_type='form', expected_status=302)
        login_data = {'email': USER['email'], 'password': USER['password']}
        self.executeReq('/login', data=login_data, cont_type='form', expected_status=302)

    def logout(self):
        self.executeReq('/logout', method='get', expected_status=302)

    def testCheckRegisteredEmail(self):
        self.executeReq('/users/{}'.format(USER['email']), method='head')

    def testCheckUnregisteredEmail(self):
        self.executeReq('/users/unkownemail@gmail.com', method='head', expected_status=400)

    def testUpdateUserProfile(self):
        self.executeReq('/users/1/profile', method='put', data=USER_PROFILE_DATA, expected_status=200)

    def testUpdateWrongUserProfile(self):
        self.logout()
        new_user = {'full_name': 'Olajide Obasan', 'email': 'jideobs@yahoo.com', 'password': 'mychora',
                    'confirm_password': 'mychora'}
        self.executeReq('/register-user', data=new_user, cont_type='form', expected_status=302)
        self.executeReq('/login', data={'email': 'jideobs@yahoo.com', 'password': 'mychora'}, expected_status=302)
        self.executeReq('/users/1/profile', method='put', data=USER_PROFILE_DATA, expected_status=400)

    def testUserProfileGet(self):
        self.executeReq('/users/1/profile', method='get')
