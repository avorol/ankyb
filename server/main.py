"""
    This file is the starting point of the whole application.
    The flask application is initialized.
"""


from time import sleep

from flask import Flask, request, render_template, url_for, redirect
from flask_restful import Api
from flask_login import LoginManager, login_user, login_required, current_user, logout_user
from models.users import Users
from config import APP_CONFIG
from webapp2_extras import security

from resources.users import UsersResource
from resources.year_book import YearBookResource
from forms import LoginForm
from forms import RegisterFormExt

app = Flask(__name__)
app.config.update(APP_CONFIG)
api = Api(app)

# app login flow.
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
login_manager.login_message = 'Please login to access this page.'


@login_manager.user_loader
def user_loader(username):
    return Users.get_by_username(username)


# handlers.
@app.route('/', methods=['GET'])
def default():
    return render_template('index.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    """
    validates user on POST, login user on successful redirecting to dashboard
    on failure renders template with errors.

    :return:
    """
    if current_user.is_authenticated:
        return redirect('dashboard')

    form = LoginForm(csrf_enabled=False)
    if request.method == 'POST' and form.validate_on_submit():
        form.user.is_authenticated = True
        form.user.put()
        login_user(form.user)
        sleep(1)
        return redirect(url_for('dashboard'), code=302)
    else:
        return render_template('login.html', form=form)


@app.route('/register', methods=['GET', 'POST'])
def register():
    """
    Handles user registration.

    it checks whether a user is not logged so as to allow registration.
    supports GET method rendering registration form.
    supports POST method validating registration form, if successful
    create user in DB afterwards log user into dashboard.
    :return:
    """
    if current_user.is_authenticated:
        return redirect('dashboard')

    form = RegisterFormExt(csrf_enabled=False)
    if request.method == 'POST' and form.validate():
        password = security.generate_password_hash(form.password.data, length=32)
        user = Users(username=form.username.data, password=password, school=form.school.data,
                     graduating_class_name=form.graduating_class_name.data, is_authenticated=True)
        user.put()
        login_user(user)
        return redirect(url_for('dashboard'), code=302)
    return render_template('register.html', form=form)


@app.route('/dashboard', methods=['GET', 'POST'])
@login_required
def dashboard():
    return render_template('dashboard.html')


@app.route('/logout', methods=['GET'])
@login_required
def logout():
    current_user.is_authenticated = False
    current_user.put()
    logout_user()
    return redirect(url_for('login'), code=302)


# RestAPI resource and routes definition
api.add_resource(UsersResource, '/users', '/users/<string:username>', '/users/<int:id>')
api.add_resource(YearBookResource, '/year-book', '/year-book/<string:owner>', '/year-book/<int:id>')

if __name__ == '__main__':
    app.run(debug=True)
