from model_base import ModelBase
from google.appengine.ext import ndb
from webapp2_extras import security
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from server.config import APP_CONFIG


SECRET = APP_CONFIG['SECRET_KEY']


class Users(ModelBase):
    """
    This Model represents a user in ankyb system with a User's properties defined below.

    The username object is the only unique property in this model.
    """
    username = ndb.StringProperty(required=True)
    password = ndb.StringProperty(required=True)
    school = ndb.StringProperty(required=True)
    graduating_class_name = ndb.StringProperty(required=True)
    is_authenticated = ndb.BooleanProperty(default=False)
    date_registered = ndb.DateTimeProperty(auto_now_add=True)
    date_last_updated = ndb.DateTimeProperty(auto_now=True)

    @property
    def is_active(self):
        """
        Required by the flask-login module to check if a particular user is active or suspended.
        :return:
        """
        return True

    @property
    def is_anonymous(self):
        """
        Required by the flask-login module to check if current_user is anonymous or not.
        :return:
        """
        return False

    def get_id(self):
        """
        Required by the flask-login module to check for the user's unique ID.
        :return:
        """
        return self.username

    @classmethod
    def get_by_username(cls, username):
        users = cls.query(cls.username == username).fetch()
        return users[0] if users else None

    def hash_password(self):
        self.password = security.generate_password_hash(self.password, length=32)

    def verify_password(self, password):
        return security.check_password_hash(password, self.password)
