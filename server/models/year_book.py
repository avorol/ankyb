from google.appengine.ext import ndb
from model_base import ModelBase


class YearBookInfo(ModelBase):
    owner = ndb.IntegerProperty()
    title = ndb.StringProperty()
    date_created = ndb.DateTimeProperty(auto_now_add=True)
    date_last_updated = ndb.DateTimeProperty(auto_now=True)


class YearBookContent(ModelBase):
    year_book = ndb.IntegerProperty(required=True)
    page_number = ndb.IntegerProperty(required=True)
    content_type = ndb.StringProperty(required=True, choices=['cover', 'articles', 'events',
                                                              'student_profiles', 'ad'])
    date_created = ndb.DateTimeProperty(auto_now_add=True)
    date_last_updated = ndb.DateTimeProperty(auto_now=True)
