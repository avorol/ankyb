from flask_wtf import Form
from wtforms.ext.appengine.ndb import model_form
from wtforms import SelectField, StringField, PasswordField, BooleanField, validators
from models.users import Users


class LoginForm(Form):
    username = StringField('Username', [validators.DataRequired(message='Please enter your username')])
    password = PasswordField('Password', [validators.DataRequired(message='Please enter your password')])
    remember_me = BooleanField('Remember me')

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self):
        """
        This function overrides the super class method still calling the super class method.
        here we validate user against the database by first getting the user, then if successful
        verifying user entered password.

        On error attach error message to the form field errors obj.

        :return: Returns a boolean value True or False depending in check outcome.
        """
        rv = Form.validate(self)
        if not rv:
            return False

        user = Users.get_by_username(self.username.data)
        if not user:
            self.username.errors.append('User does not exist')
            return False

        if not user.verify_password(self.password.data):
            self.password.errors.append('Password incorrect')
            return False
        self.user = user
        return True


RegisterForm = model_form(Users, Form)


class RegisterFormExt(RegisterForm):
    password = PasswordField('Password', [validators.DataRequired(),
                                          validators.EqualTo('confirm_password', message='Password does not match')])
    confirm_password = PasswordField('Confirm Password', [validators.DataRequired()])
    school = SelectField('School', [validators.DataRequired()],
                         choices=[('', 'Select school'), ('LASU', 'Lagos State University'),
                                  ('OOU', 'Olabisi Onabanjo University')])

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)

    def validate(self):
        """
        This function overrides the super class method still calling the super class method.
        here we validate user against the database by first getting the user, then if successful
        verifying user entered password.

        On error attach error message to the form field errors obj.

        :return: Returns a boolean value True or False depending in check outcome.
        """
        rv = Form.validate(self)
        if not rv:
            return False

        user = Users.get_by_username(self.username.data)
        if user:
            self.username.errors.append('Username has already been registered')
            return False
        return True
