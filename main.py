from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)


class GraduatingClasses(Resource):
    def get(self):
        pass

    def post(self):
        pass


class StudentProfiles(Resource):
    def get(self):
        pass

    def post(self):
        pass

    def put(self):
        pass


class YearBooks(Resource):
    def get(self):
        pass

    def post(self):
        pass

    def put(self):
        pass


api.add_resource(GraduatingClasses, '/graduating-class')
api.add_resource(StudentProfiles, '/student-profiles')
api.add_resource(YearBooks, '/year-books')

if __name__ == '__main__':
    app.run(debug=True)
